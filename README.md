# Testaufgabe 01

## Tasks

Anhand der angegebenen Links soll eine Produktuebersichtsseite geschrieben werden.
Die Daten dazu sollen von der Seite geholt und aus dem json-objekt genutzt werden.

Jedes Produkt auf der Produktuebersicht soll auf eine detaillierte Ansicht des Produkts verlinken.


## todos
* test schreiben
* komponenten in eigene dateien schieben
  * components/ProductOverview
  * components/ProductDetails
* state zu den passenden komponenten umziehen



* produkt detail seite ✓
* das bild, der preis und der name ✓
* produktuebersichtseite ✓
* die ganzen produkte als text only ✓
* soll in einem grid angezeigt werden ✓
