import { useState, useEffect } from 'react'
import { baseUrl, getProducts, getProductDetails } from './api'
import './App.css'

function App() {
  const PRODUCT_OVERVIEW = 'product-overview'
  const PRODUCT_DETAIL = 'product-detail'

  const [ productList, setProductList ] = useState([])
  const [ displayComponent, setDisplayComponent ] = useState(PRODUCT_OVERVIEW)
  const [ detailLink, setDetailLink ] = useState('')
  const [ productDetails, setProductDetails ] = useState({})

  useEffect( () => {
    getProducts(setProductList)
  }, [])

  useEffect( () => {
    getProductDetails(detailLink, setProductDetails)
  }, [detailLink])

  const productListElements = productList.map(product => {
    return <li
        key={product.name}
        className="grid-item"
        onClick={()=>{
          setDisplayComponent(PRODUCT_DETAIL)
          setDetailLink(product.product_url)
        }}
      >
        {product.name}
      </li>
  })

  const ProductOverviewPage = () => {
    return <ul className="grid-container">
        {productListElements}
      </ul>
  }


  const ProductDetailPage = () => {
    return <div onClick={() => setDisplayComponent(PRODUCT_OVERVIEW)}>
        <div>{productDetails.name}</div>
        <div>{productDetails.price}</div>
        <img src={`${baseUrl}${productDetails.photo_url}`} alt={productDetails.name} />
      </div>
  }

  return (
    <div className="App">
    {displayComponent === PRODUCT_OVERVIEW && <ProductOverviewPage />}
    {displayComponent === PRODUCT_DETAIL && <ProductDetailPage />}
    </div>
  );
}

export default App;
