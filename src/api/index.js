const baseUrl = 'https://api.predic8.de'

const getProducts = (callback) => {
  fetch(`${baseUrl}/shop/products/`)
    .then(response => response.json())
    .then(data => callback(data.products));
}

const getProductDetails = (path, callback) => {
  fetch(`${baseUrl}${path}`)
    .then(response => response.json())
    .then(data => callback(data));
}

export { getProducts, getProductDetails, baseUrl }
